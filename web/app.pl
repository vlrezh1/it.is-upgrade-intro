#!/usr/bin/perl -wT
use DBI;
use Time::HiRes qw(gettimeofday tv_interval);

my $dbname = "mydb";  
my $host = "db";  
my $port = 3306;  
my $username = "rootuser";  
my $password = "rootpass"; 

my $dbh = DBI -> connect("dbi:mysql:dbname=$dbname;host=$host;port=$port", $username, $password);

print "Content-type: text/plain\n\n";

my $SQL = "SELECT id, name FROM test_table";

$start = [gettimeofday];
my $sth = $dbh->prepare($SQL);
$sth -> execute();
$time_passed = tv_interval ($start);

while (my @row = $sth->fetchrow_array)
{  
    print "$row[0]\t$row[1]\n";
}
print "Executed for $time_passed seconds";

$sth->finish();
$dbh -> disconnect;
